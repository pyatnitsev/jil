<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:homepage.html.twig');
    }

    public function getListAction()
    {
        $rss_feed = $this->getDoctrine()->getRepository('AppBundle:Rss')->findBy(array(), array('updated' => 'DESC'));
        $response = new JsonResponse();
        $response->setData(json_encode($this->getFeedData($rss_feed)));
        return $response;
    }

    public function postCommentAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $rss_item = $em->getRepository('AppBundle:Rss')->find((int)$request->request->get('relatedElement', 0));
        $commentDAO = new Comment();
        $commentDAO->setComment($request->request->get('comment-field', null));
        $commentDAO->setEmail($request->request->get('email', null));
        $commentDAO->setItem($rss_item);
        $em->persist($commentDAO);
        $em->flush();

        return new JsonResponse('Success');

    }

    private function getFeedData($rss_feed)
    {
        $result_feed = null;
        foreach ($rss_feed as $item) {
            $rss_item = null;
            $rss_item['id'] = $item->getId();
            $rss_item['Title'] = $item->getTitle();
            $rss_item['Description'] = strip_tags($item->getDescription());
            $rss_item['Media'] = $item->getMediaBlob();
            $rss_item['Link'] = $item->getLink();
            $rss_item['Updated'] = $item->getUpdated()->format('d.m.Y H:s:i');

            foreach ($item->getComments() as $comment) {
                $commentResult['email'] = $comment->getEmail();
                $commentResult['comment'] = $comment->getComment();
                $rss_item['Comments'][] = $commentResult;
            }

            $result_feed[] = $rss_item;
        }
        return $result_feed;
    }
}

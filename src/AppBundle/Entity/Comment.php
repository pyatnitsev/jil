<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 22.03.2016
 * Time: 16:00
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comment")
 */
class Comment
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $email;
    /**
     * @ORM\Column(type="string", length=1000)
     */
    protected $comment;
    /**
     * @ORM\ManyToOne(targetEntity="Rss", inversedBy="comments")
     * @ORM\JoinColumn(name="rss_item_id", referencedColumnName="id")
     */
    protected $item;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Comment
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Rss $item
     *
     * @return Comment
     */
    public function setItem(\AppBundle\Entity\Rss $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Rss
     */
    public function getItem()
    {
        return $this->item;
    }
}

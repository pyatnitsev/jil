<?php
/**
 * Created by PhpStorm.
 * User: Danil
 * Date: 22.03.2016
 * Time: 16:00
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="rss_item")
 */
class Rss
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $title;
    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $summary;
    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $description;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;
    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $publicId;
    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $link;
    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $author;
    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $commentRss;
    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $additional;
    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $mediaBlob;
    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $categoryBlob;
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="item")
     */
    protected $comments;
    /**
     * @ORM\ManyToOne(targetEntity="Channel", inversedBy="items", cascade={"all"})
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     */
    protected $channel;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastUpdate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Rss
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Rss
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Rss
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Rss
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set publicId
     *
     * @param string $publicId
     *
     * @return Rss
     */
    public function setPublicId($publicId)
    {
        $this->publicId = $publicId;

        return $this;
    }

    /**
     * Get publicId
     *
     * @return string
     */
    public function getPublicId()
    {
        return $this->publicId;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Rss
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Rss
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set commentRss
     *
     * @param string $commentRss
     *
     * @return Rss
     */
    public function setCommentRss($commentRss)
    {
        $this->commentRss = $commentRss;

        return $this;
    }

    /**
     * Get commentRss
     *
     * @return string
     */
    public function getCommentRss()
    {
        return $this->commentRss;
    }

    /**
     * Set additional
     *
     * @param string $additional
     *
     * @return Rss
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;

        return $this;
    }

    /**
     * Get additional
     *
     * @return string
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Set mediaBlob
     *
     * @param string $mediaBlob
     *
     * @return Rss
     */
    public function setMediaBlob($mediaBlob)
    {
        $this->mediaBlob = $mediaBlob;

        return $this;
    }

    /**
     * Get mediaBlob
     *
     * @return string
     */
    public function getMediaBlob()
    {
        return $this->mediaBlob;
    }

    /**
     * Set categoryBlob
     *
     * @param string $categoryBlob
     *
     * @return Rss
     */
    public function setCategoryBlob($categoryBlob)
    {
        $this->categoryBlob = $categoryBlob;

        return $this;
    }

    /**
     * Get categoryBlob
     *
     * @return string
     */
    public function getCategoryBlob()
    {
        return $this->categoryBlob;
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Rss
     */
    public function addComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set channel
     *
     * @param \AppBundle\Entity\Channel $channel
     *
     * @return Rss
     */
    public function setChannel(\AppBundle\Entity\Channel $channel = null)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return \AppBundle\Entity\Channel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Rss
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }
}

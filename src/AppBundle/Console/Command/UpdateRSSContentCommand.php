<?php

namespace AppBundle\Console\Command;

use AppBundle\Entity\Channel;
use AppBundle\Entity\Rss;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Controller\APIController;
use Symfony\Component\Validator\Constraints\DateTime;

class UpdateRSSContentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rss:update')
            ->setDescription('This command fetch new data from rss channel')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'url of rss channel'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        // create reader and try to fetch data from rss by url that passed in console.
        $reader = $this->getContainer()->get('debril.reader');
        $runTime = new \DateTime();
        $feed = $reader->getFeedContent($input->getArgument('url'));


        $channel = $entityManager->getRepository('AppBundle:Channel')->findOneBy(array('publicId' => $feed->getPublicId()));
        // if channel not in database yet
        if (!$channel) {
            $channel = new Channel();
            $channel->setDescription($feed->getDescription());
            $channel->setLastModified($feed->getLastModified());
            $channel->setLink($feed->getLink());
            $channel->setPublicId($feed->getPublicId());
            $channel->setTitle($feed->getTitle());

            foreach ($feed->getItems() as $item) {
                $channel->addItem($this->fillRssObject($runTime, $item));
            }
            $entityManager->persist($channel);
            $entityManager->flush();
        } else // if channel exists in db.
        {
            foreach ($feed->getItems() as $item) {
                $itemInDb = $entityManager->getRepository('AppBundle:Rss')->findOneBy(array('updated' => $item->getUpdated()));
                if ($itemInDb) {
                    $itemInDb->setLastUpdate($runTime);
                    $entityManager->persist($itemInDb);
                    $entityManager->flush();
                } else {
                    $channel->addItem($this->fillRssObject($runTime, $item));
                }
            }
            $entityManager->persist($channel);
            $entityManager->flush();
        }
        $query = $entityManager->createQuery(
            'SELECT R
             FROM AppBundle:Rss R
             WHERE R.lastUpdate != :current_update'
        )->setParameter('current_update', $runTime);

        $result = $query->getResult();
        foreach ($result as $item) {

            foreach ($item->getComments() as $comment) {
                $entityManager->remove($comment);
                $entityManager->flush();

            }

            $entityManager->remove($item);
            $entityManager->flush();
        }

        $output->writeln('done');
    }

    private function fillRssObject($runTime, $feedItemObject)
    {
        $rssItem = new Rss();
        $rssItem->setLastUpdate($runTime);
        $rssItem->setTitle($feedItemObject->getTitle());
        $rssItem->setSummary($feedItemObject->getSummary());
        $rssItem->setDescription($feedItemObject->getDescription());
        $rssItem->setUpdated($feedItemObject->getUpdated());
        $rssItem->setPublicId($feedItemObject->getPublicId());
        $rssItem->setLink($feedItemObject->getLink());
        $rssItem->setAuthor($feedItemObject->getAuthor());
        $rssItem->setCommentRss($feedItemObject->getComment());
        $medias = null;
        foreach ($feedItemObject->getMedias() as $media) {
            $mediaFile = null;
            $mediaFile['type'] = $media->getType();
            $mediaFile['length'] = $media->getLength();
            $mediaFile['url'] = $media->getUrl();

            $medias[] = $mediaFile;
        }
        $rssItem->setMediaBlob(json_encode($medias));
        return $rssItem;
    }
}
$(document).ready(function () {
    $(document).ajaxStop(function () {
        $(".se-pre-con").fadeOut("slow");
    });
    var rss_item = $('#rss-item-with-comment-mark').clone();
    $('#rss-item-with-comment-mark').remove();
    $(rss_item).show();
    var comment_item = $(rss_item).find('#comment').clone();
    $(rss_item).find('.media').remove();
    $.ajax({
        url: '/ajax/get',
        dataType: "json",
        success: function (data, textStatus) {
            data = $.parseJSON(data);
            $.each(data, function (i, val) {
                $(rss_item).find('.list-group-item-heading').text(val['Title']);
                $(rss_item).find('.list-group-item-heading-link').attr('href', val['Link']).attr('target', '_blanc');
                $(rss_item).find('.list-group-item-text').text(val['Description']);
                $(rss_item).find('.published-at').text(val['Updated']);
                $(rss_item).attr('id', val['id']);
                $('.list-group').append(rss_item.clone());
                if (val['Comments'] != null) {
                    $.each(val['Comments'], function (j, comment) {
                        $(comment_item).find('.media-heading').text(comment['email']);
                        $(comment_item).find('.comment-body').text(comment['comment']);
                        $('#' + val['id']).find('.list-group-item').append(comment_item.clone());
                    });
                }

            });
        }
    });
    $('#comment-form').on('show.bs.modal', function (e) {
        var invoker = $(e.relatedTarget);
        $(this).find('.related-element').val($(invoker).parent().parent().attr('id'));

    });
    $("form.comment").submit(function (e) {
        e.preventDefault();
    });
    $('.submit-btn').click(function () {
        $.ajax({
            type: "POST",
            url: "/ajax/post-comment/",
            data: $('form.comment').serialize(),
            success: function (msg) {

                var targetId = $('form.comment').find('.related-element').val();
                var newComment = $(comment_item).clone();
                $(newComment).find('.media-heading').text($('#comment-form').find('#email').val());
                $(newComment).find('.comment-body').text($('#comment-form').find('#comment-field').val());
                $('#' + targetId).find('.list-group-item').append(newComment);
                $('#comment-form').modal('hide');
                $('#comment-form').find('#email').val('');
                $('#comment-form').find('#comment-field').val('')


            },
            error: function () {
                alert("failure");
            }
        });
    });
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}